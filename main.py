import logging
from typing import Optional
from fastapi import FastAPI, Header, Form
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse, HTMLResponse


server = FastAPI()


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="./main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))


@server.get('/', tags=["Main"])
def home(request: Request,
        login: str):
    print(f'client=\"{request.client.host}:{request.client.port}\" login = {login}')
    return RedirectResponse("https://afisha.yandex.ru/moscow/concert/glavnyi-novogodnii-kontsert-2023")
